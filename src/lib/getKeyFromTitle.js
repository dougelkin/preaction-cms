export default (title) => title.toLowerCase().replace(/[^A-z0-9]/gi, '-')
