export default [
  [{ header: [] }, { font: [] }],
  [{ size: ['small', false, 'large', 'huge'] }],
  ['bold', 'italic', 'strike'],
  [
    'blockquote',
    { indent: '-1' },
    { indent: '+1' },
    { list: 'bullet' },
    { list: 'ordered' },
  ],
  [{ align: [] }],
  ['link'],
]
