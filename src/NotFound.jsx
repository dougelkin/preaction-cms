import React from 'react'

class NotFound extends React.Component {
  render() {
    return (
      <div className='not-found'>
        <h1>Sorry! That Page is not found!</h1>
        <p>Try visiting one of the links on this page.</p>
      </div>
    )
  }
}

export default NotFound
